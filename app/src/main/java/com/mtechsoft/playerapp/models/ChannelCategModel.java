package com.mtechsoft.playerapp.models;

public class ChannelCategModel {
  String ChannelCategName;
  int channel_category_id;

  public int getChannel_category_id() {
    return channel_category_id;
  }

  public void setChannel_category_id(int channel_category_id) {
    this.channel_category_id = channel_category_id;
  }

  public String getChannelCategName() {
        return ChannelCategName;
    }

    public void setChannelCategName(String channelCategName) {
        ChannelCategName = channelCategName;
    }

}

