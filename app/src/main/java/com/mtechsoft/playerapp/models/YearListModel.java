package com.mtechsoft.playerapp.models;

public class YearListModel {
    String year="";
    int id = 0;

    public YearListModel(String year, int id) {
        this.year = year;
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
