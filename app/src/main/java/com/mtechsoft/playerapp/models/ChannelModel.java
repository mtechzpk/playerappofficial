package com.mtechsoft.playerapp.models;

public class ChannelModel {
  String photo="";
  int id;
  String channelName;
  String channel_Category_id;

  public ChannelModel(String photo, int id, String channelName) {
    this.photo = photo;
    this.id = id;
    this.channelName = channelName;
  }

  public String getChannelName() {
    return channelName;
  }

  public void setChannelName(String channelName) {
    this.channelName = channelName;
  }

  public String getChannel_Category_id() {
    return channel_Category_id;
  }

  public void setChannel_Category_id(String channel_Category_id) {
    this.channel_Category_id = channel_Category_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }



}

