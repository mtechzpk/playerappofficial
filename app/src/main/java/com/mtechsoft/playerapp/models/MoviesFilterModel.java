package com.mtechsoft.playerapp.models;

public class MoviesFilterModel {
  int id;
  String MovieName;
  String url;
  String thumbnail;

  public MoviesFilterModel(int id, String movieName, String url, String thumbnail) {
    this.id = id;
    this.MovieName = movieName;
    this.url = url;
    this.thumbnail = thumbnail;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMovieName() {
    return MovieName;
  }

  public void setMovieName(String MovieName) {
    this.MovieName = MovieName;
  }
}

