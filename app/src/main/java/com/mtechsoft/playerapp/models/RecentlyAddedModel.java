package com.mtechsoft.playerapp.models;

public class RecentlyAddedModel {
  String photo="";
  int id;
  String url;
  String episodeNo;
String url_type;

  public RecentlyAddedModel(String photo, int id, String url, String episodeNo, String url_type) {
    this.photo = photo;
    this.id = id;
    this.url = url;
    this.episodeNo = episodeNo;
    this.url_type = url_type;
  }

  public String getUrl_type() {
    return url_type;
  }

  public void setUrl_type(String url_type) {
    this.url_type = url_type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEpisodeNo() {
    return episodeNo;
  }

  public void setEpisodeNo(String episodeNo) {
    this.episodeNo = episodeNo;
  }
}


