package com.mtechsoft.playerapp.models;

public class LatestMoviesModel {

  int id;
  String MovieName;
  String url;
  String thumbnail;
String url_type;

  public LatestMoviesModel(int id, String movieName, String url, String thumbnail, String url_type) {
    this.id = id;
    MovieName = movieName;
    this.url = url;
    this.thumbnail = thumbnail;
    this.url_type = url_type;
  }

  public String getUrl_type() {
    return url_type;
  }

  public void setUrl_type(String url_type) {
    this.url_type = url_type;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMovieName() {
    return MovieName;
  }

  public void setMovieName(String movieName) {
    MovieName = movieName;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }
}

