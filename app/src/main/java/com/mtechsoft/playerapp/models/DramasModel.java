package com.mtechsoft.playerapp.models;

public class DramasModel {
  String DramaName;
  String photo="";
  int dramaId;
  String totalEpisodes;

  public DramasModel(String dramaName, String photo, int dramaId, String totalEpisodes) {
    DramaName = dramaName;
    this.photo = photo;
    this.dramaId = dramaId;
    this.totalEpisodes = totalEpisodes;
  }

  public int getDramaId() {
    return dramaId;
  }

  public void setDramaId(int dramaId) {
    this.dramaId = dramaId;
  }

  public String getTotalEpisodes() {
    return totalEpisodes;
  }

  public void setTotalEpisodes(String totalEpisodes) {
    this.totalEpisodes = totalEpisodes;
  }

  public String getDramaName() {
    return DramaName;
  }

  public void setDramaName(String dramaName) {
    DramaName = dramaName;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

}

