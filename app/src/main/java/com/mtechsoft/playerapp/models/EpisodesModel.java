package com.mtechsoft.playerapp.models;

public class EpisodesModel {
  String photo="";
  int id;
  String episodeNo;
  String urlType;

  public EpisodesModel(String photo, int id, String episodeNo, String urlType) {
    this.photo = photo;
    this.id = id;
    this.episodeNo = episodeNo;
    this.urlType = urlType;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEpisodeNo() {
    return episodeNo;
  }

  public void setEpisodeNo(String episodeNo) {
    this.episodeNo = episodeNo;
  }

  public String getPhoto() {
    return photo;
  }

  public String getUrlType() {
    return urlType;
  }

  public void setUrlType(String urlType) {
    this.urlType = urlType;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

}

