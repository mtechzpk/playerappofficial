package com.mtechsoft.playerapp.models;

public class MoviesTypeModel {
String moviesCategoriesName;
int movies_cat_id;

  public String getMoviesCategoriesName() {
    return moviesCategoriesName;
  }

  public void setMoviesCategoriesName(String moviesCategoriesName) {
    this.moviesCategoriesName = moviesCategoriesName;
  }

  public int getMovies_cat_id() {
    return movies_cat_id;
  }

  public void setMovies_cat_id(int movies_cat_id) {
    this.movies_cat_id = movies_cat_id;
  }
}

