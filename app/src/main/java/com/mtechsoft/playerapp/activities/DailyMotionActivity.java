package com.mtechsoft.playerapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;


import com.dailymotion.android.player.sdk.PlayerWebView;
import com.mtechsoft.playerapp.R;

import java.util.HashMap;
import java.util.Map;

public class DailyMotionActivity extends AppCompatActivity {
    private PlayerWebView mVideoView;
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_daily_motion);

        Intent intent = getIntent();
        url = intent.getStringExtra("data");

        mVideoView =  findViewById(R.id.video_title);
        mVideoView.load(url);





    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mVideoView.destroy();
    }
}
