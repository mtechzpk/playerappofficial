package com.mtechsoft.playerapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.BuildConfig;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.navigation.NavigationView;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.SessionManager.SessionManager;
import com.mtechsoft.playerapp.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public NavController navController;
    ImageView ivDrawer;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar;
    TextView tvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ivDrawer = findViewById(R.id.ivDrawer);
        rlToolbar = findViewById(R.id.rlToolbar);
        tvTitle = findViewById(R.id.tvTitle);


        String namee = Utilities.getString(MainActivity.this, "user_name");
        String emailll = Utilities.getString(MainActivity.this, "user_email");

        View hView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        TextView nameeee = hView.findViewById(R.id.user_name);
        TextView emaillll = hView.findViewById(R.id.user_email);

        nameeee.setText(namee);
        emaillll.setText(emailll);



        initNavigation();
    }

    private void initNavigation() {
        navigationView.setItemIconTintList(null);
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        setColor();

        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                        rlToolbar.setVisibility(View.VISIBLE);
                        tvTitle.setText(destination.getLabel());


                }

            }
        });
        navigationView.getMenu().findItem(R.id.nav_help).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                showExitAppAlert();
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.nav_app).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                shareApp();

                return true;
            }
        });

    }

    public void shareApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

//    private void SignupApi() {
////        String token = FirebaseInstanceId.getInstance().getToken();
//        final ProgressDialog progressdialog = new ProgressDialog(this);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.show();
//        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new com.android.volley.Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                try {
//                    JSONObject object = new JSONObject(response);
//                    String status = object.getString("success");
//                    if (status.equals("1")) {
//
//
//                        progressdialog.dismiss();
//                        showExitAppAlert();
//
//
//                    } else {
//                        progressdialog.dismiss();
//
//                        if (MainActivity.this != null) {
//                            String error = object.getString("message");
//                            Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                progressdialog.dismiss();
//                String message = null;
//                if (volleyError instanceof NetworkError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                } else if (volleyError instanceof AuthFailureError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ParseError) {
//                    message = "Parsing error! Please try again after some time!!";
//                } else if (volleyError instanceof NoConnectionError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof TimeoutError) {
//                    message = "Connection TimeOut! Please check your internet connection.";
//                }
//                if (MainActivity.this != null)
//                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//
//                params.put("do", "logout");
//                params.put("id", current_user_id);
//                params.put("apikey", "mtechapi12345");
//                return params;
//            }
//        };
//
//        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
//                25000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        MySingleton.getInstance(MainActivity.this).addToRequestQueue(RegistrationRequest);
//
//
//    }
    private boolean flagDoubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {

        if (!navController.getCurrentDestination().getLabel().toString().equals("Dashboard")) {


            super.onBackPressed();
        } else {
            if (flagDoubleBackToExitPressedOnce) {

                super.onBackPressed();
                return;
            }
            this.flagDoubleBackToExitPressedOnce = true;
            Toast.makeText(this, "press again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    flagDoubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    private void showExitAppAlert() {
        new AlertDialog.Builder(this)
                .setTitle("Quit")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SessionManager sessionManager = new SessionManager(MainActivity.this);
                        sessionManager.logoutUser();
                        Utilities.clearSharedPref(MainActivity.this);

                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void setColor(){

        Menu menu = navigationView.getMenu();
        MenuItem tools= menu.findItem(R.id.homeFragment);
        SpannableString s = new SpannableString(tools.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s.length(), 0);
        tools.setTitle(s);

        Menu menu1 = navigationView.getMenu();
        MenuItem tools1= menu1.findItem(R.id.moviesFragment);
        SpannableString s1 = new SpannableString(tools1.getTitle());
        s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
        tools1.setTitle(s1);

        Menu menu2 = navigationView.getMenu();
        MenuItem tools2= menu2.findItem(R.id.dramaFragment);
        SpannableString s2 = new SpannableString(tools2.getTitle());
        s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
        tools2.setTitle(s2);

//        Menu menu3 = navigationView.getMenu();
//        MenuItem tools3= menu3.findItem(R.id.nav_rate_us);
//        SpannableString s3 = new SpannableString(tools3.getTitle());
//        s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
//        tools3.setTitle(s3);

        Menu menu4 = navigationView.getMenu();
        MenuItem tools4= menu4.findItem(R.id.nav_help);
        SpannableString s4 = new SpannableString(tools4.getTitle());
        s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
        tools4.setTitle(s4);


//        Menu menu6 = navigationView.getMenu();
//        MenuItem tools6= menu6.findItem(R.id.nav_fav);
//        SpannableString s6= new SpannableString(tools6.getTitle());
//        s6.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s6.length(), 0);
//        tools6.setTitle(s6);

        Menu menu7 = navigationView.getMenu();
        MenuItem tools7= menu7.findItem(R.id.nav_app);
        SpannableString s7= new SpannableString(tools7.getTitle());
        s7.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s7.length(), 0);
        tools7.setTitle(s7);

    }
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);

        // change color for icon 0
        Drawable yourdrawable = menu.getItem(0).getIcon(); // change 0 with 1,2 ...
        yourdrawable.mutate();
        yourdrawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
        return true;
    }
}
