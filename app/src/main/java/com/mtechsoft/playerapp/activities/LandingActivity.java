package com.mtechsoft.playerapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.navigation.NavigationView;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.SessionManager.SessionManager;

public class LandingActivity extends AppCompatActivity {
    public NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggedIn()) {
            startActivity(new Intent(LandingActivity.this, MainActivity.class));
            finish();
        }


        initNavigation();
    }

    private void initNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);


    }
}
