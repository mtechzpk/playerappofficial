package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.MainActivity;
import com.mtechsoft.playerapp.adapters.AllChannelAdapter;
import com.mtechsoft.playerapp.models.ChannelModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChannelFragment extends Fragment implements AllChannelAdapter.Callback{
    RecyclerView rvChannels;
    ArrayList<ChannelModel> channelModels;
    int chanel_cat_dd;
    JSONObject jsonObject;
    JSONArray objUser;

    public static int chanel_id;
    AllChannelAdapter pAdapter;



    Context context;

    public ChannelFragment() {
        // Required empty public constructor
    }

    View view;





    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_channel, container, false);
        initViews();

        channelModels = new ArrayList<>();


        getChannels();




        return view;
    }

    public void initViews() {

        rvChannels = view.findViewById(R.id.rvChannels);


    }
//    @Override
//    public void onItemClick(int pos) {
//
//        ((MainActivity) getActivity()).navController.navigate(R.id.action_channelFragment_to_dramaFragment);
//    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getChannels() {


        chanel_cat_dd=Utilities.getInt(getContext(),"Ch_cat_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.CHANNEL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
//                    JSONObject object = new JSONObject(response);
//                    int status = object.getInt("status");
//                    String msg = object.getString("message");
//                    if (status == 200) {
//
//                        // JSONObject obj = object.getJSONObject("result");
//                        final JSONArray objUser = object.getJSONArray("data");
//
//                        for (int i = 0; i < objUser.length(); i++) {
////
//                            JSONObject jsonObject = objUser.getJSONObject(i);
//
//                            int id = jsonObject.getInt("id");
//                            String channel_name = jsonObject.getString("channel_name");
//                            String channel_category_id = jsonObject.getString("channel_category_id");
//                            String channel_photo = jsonObject.getString("filename");
//
//                            channelModelll.setId(id);
//                            channelModelll.setChannelName(channel_name);
//                            channelModelll.setChannel_Category_id(channel_category_id);
//                            channelModelll.setPhoto(channel_photo);
//
//                            channelModels.add(channelModelll);



                            channelModels = new ArrayList<ChannelModel>();
                            JSONObject object = new JSONObject(response);
                            int status = object.getInt("status");
                            String msg = object.getString("message");
                            if (status == 200) {

                                progressdialog.dismiss();
                                objUser = object.getJSONArray("data");
                                for (int i = 0; i < objUser.length(); i++) {
                                    jsonObject = objUser.getJSONObject(i);
                                    int id = jsonObject.getInt("id");
                                    String channel_name = jsonObject.getString("channel_name");
                                    String thumbnail = jsonObject.getString("filename");
                                    channelModels.add(new ChannelModel(thumbnail,id,channel_name));


                                }

                        pAdapter = new AllChannelAdapter(channelModels,getContext(),ChannelFragment.this);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),2,RecyclerView.VERTICAL,false);
                        rvChannels.setLayoutManager(layoutManager);
                        rvChannels.setAdapter(pAdapter);
//                        rvChannels.getRecycledViewPool().clear();
//                        pAdapter.notifyDataSetChanged();


                    } else {

                        Toast.makeText(getActivity(), "No Channel Avilable", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("channel_category_id", String.valueOf(chanel_cat_dd));
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }


    @Override
    public void onItemClick(int pos) {

        int idd = channelModels.get(pos).getId();
        Utilities.saveInt(getContext(), "ch_id", idd);
        ((MainActivity) getActivity()).navController.navigate(R.id.action_channelFragment_to_dramaFragment);

    }
}
