package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.LandingActivity;
import com.mtechsoft.playerapp.api.ApiModelClass;
import com.mtechsoft.playerapp.api.ServerCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment implements View.OnClickListener {
    Button bSignup;
    LinearLayout llLogin;
    EditText etName, etEmail, etPass, etConfirmPass;


    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        initviews(view);
        return view;
    }


    public void initviews(View v) {
        llLogin = v.findViewById(R.id.llLogin);
        bSignup = v.findViewById(R.id.bSignup);

        etName = v.findViewById(R.id.etName);
        etEmail = v.findViewById(R.id.etEmail);
        etPass = v.findViewById(R.id.etPassword);
        etConfirmPass = v.findViewById(R.id.etConfirmPassword);
        initNav();

    }

    public void initNav() {
        llLogin.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_signUpFragment_to_loginFragment));
//        bSignup.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_signUpFragment_to_loginFragment));
        bSignup.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSignup:

                String userName = etName.getText().toString();
                String userEmail = etEmail.getText().toString();
                String userPassword = etPass.getText().toString();
                String confirmPassword = etConfirmPass.getText().toString();

                if (!userName.isEmpty()) {
                    if (!userEmail.isEmpty()) {
                        if (!userPassword.isEmpty()) {
                            if (!confirmPassword.isEmpty()) {
                                if (userPassword.equals(confirmPassword)) {

                                    RegistrationApi(userName, userEmail, userPassword, confirmPassword);

                                } else {
                                    etConfirmPass.setError("password not match");

                                }


                            } else {

                                Toast.makeText(getContext(), "required passowrd is not match", Toast.LENGTH_SHORT).show();
                            }


                        } else {

                            Toast.makeText(getActivity(), "Password Required", Toast.LENGTH_SHORT).show();

                        }


                    } else {

                        Toast.makeText(getActivity(), "Email required", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(getActivity(), "Name required", Toast.LENGTH_SHORT).show();
                }


                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void RegistrationApi(final String name, final String email, final String pass, final String con_pass) {
        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();

        Map<String, String> postParam = new HashMap<String, String>();

        postParam.put("name", name);
        postParam.put("email", email);
        postParam.put("password", pass);
        postParam.put("password_confirmation", con_pass);

        HashMap<String, String> headers = new HashMap<String, String>();

        headers.put("Accept", "application/json");

        ApiModelClass.GetApiResponse(Request.Method.POST, Server.SIGNUP, getContext(), postParam, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {


                if (ERROR.isEmpty()) {

                    try {
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("status");
                        if (status == 200) {

                            JSONObject data = object.getJSONObject("data");
                            String name = data.getString("name");
                            String email = data.getString("email");
                            int id = data.getInt("id");

                            Utilities.saveString(getContext(), "user_name", name);
                            Utilities.saveString(getContext(), "user_email", email);
                            Utilities.saveInt(getContext(), "user_id", id);

                            String msg = object.getString("message");
                            Utilities.hideProgressDialog();
                            progressdialog.dismiss();
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            ((LandingActivity) getActivity()).navController.navigate(R.id.action_signUpFragment_to_loginFragment);


                        } else{
                            String msgg = object.getString("message");
                            Utilities.hideProgressDialog();
                            Toast.makeText(getActivity(), msgg, Toast.LENGTH_SHORT).show();
                        }
                        

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Utilities.hideProgressDialog();
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });


    }


}
