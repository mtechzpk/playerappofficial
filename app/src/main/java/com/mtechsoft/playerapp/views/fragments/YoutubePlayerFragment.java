//package com.mtechsoft.playerapp.views.fragments;
//
//
//import android.content.Intent;
//import android.os.Bundle;
//
//import androidx.fragment.app.Fragment;
//
//import android.provider.SyncStateContract;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Toast;
//
//
//import com.mtechsoft.playerapp.R;
//import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
//
//import io.paperdb.Paper;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class YoutubePlayerFragment extends Fragment {
//
//
//    public YoutubePlayerFragment() {
//        // Required empty public constructor
//    }
//
//    private View view;
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        view = inflater.inflate(R.layout.fragment_youtube_player, container, false);
//        Paper.init(getActivity());
//        YouTubePlayerView youTubePlayerView = view.findViewById(R.id.youtube_player_view);
//        getLifecycle().addObserver(youTubePlayerView);
//
//
//        String url = Paper.book().read("videourl");
//        Intent intent = new Intent(this, SampleAppTemplateActivity.class);
//
//        intent.putExtra(SyncStateContract.Constants.TITLE.name(), getString(R.string.app_name));
//        intent.putExtra(SyncStateContract.Constants.GITHUB_URL.name(), "https://github.com/PierfrancescoSoffritti/android-youtube-player/");
//        intent.putExtra(SyncStateContract.Constants.HOMEPAGE_URL.name(), "https://pierfrancescosoffritti.github.io/android-youtube-player/");
//
//        ExampleActivityDetails[] examples = new ExampleActivityDetails[]{
//                new ExampleActivityDetails(R.string.simple_example,null, SimpleExampleActivity.class),
//                new ExampleActivityDetails(R.string.complete_example,null, CompleteExampleActivity.class),
//                new ExampleActivityDetails(R.string.web_ui_example,null, WebUiExampleActivity.class),
//                new ExampleActivityDetails(R.string.custom_ui_example,null, CustomUiActivity.class),
//                new ExampleActivityDetails(R.string.recycler_view_example,null, RecyclerViewActivity.class),
//                new ExampleActivityDetails(R.string.view_pager_example,null, ViewPagerActivity.class),
//                new ExampleActivityDetails(R.string.fragment_example,null, FragmentExampleActivity.class),
//                new ExampleActivityDetails(R.string.live_video_example,null, LiveVideoActivity.class),
//                new ExampleActivityDetails(R.string.player_status_example,null, PlayerStateActivity.class),
//                new ExampleActivityDetails(R.string.picture_in_picture_example,null, PictureInPictureActivity.class),
//                new ExampleActivityDetails(R.string.chromecast_example,null, ChromeCastExampleActivity.class),
//                new ExampleActivityDetails(R.string.iframe_player_options_example,null, IFramePlayerOptionsExampleActivity.class),
//                new ExampleActivityDetails(R.string.no_lifecycle_observer_example,null, NoLifecycleObserverExampleActivity.class)
//        };
//
//        intent.putExtra(Constants.EXAMPLES.name(), examples);
//
//        startActivity(intent);
//        finish();
//        playVideo(url, youTubePlayerView);
//        return view;
//    }
//
//    public void playVideo(final String videoId, YouTubePlayerView youTubePlayerView) {
//        //initialize youtube player view
//        youTubePlayerView.initialize("AIzaSyAze80NcjSR50BFLA8y0hj3mqggHYcLvcQ",
//                new YouTubePlayer.OnInitializedListener() {
//                    @Override
//                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
//                                                        YouTubePlayer youTubePlayer, boolean b) {
//                        youTubePlayer.cueVideo(videoId);
//                        Toast.makeText(getActivity(), "onInitializationSuccess", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onInitializationFailure(YouTubePlayer.Provider provider,
//                                                        YouTubeInitializationResult youTubeInitializationResult) {
//                        Toast.makeText(getActivity(), "onInitializationFailure", Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//    }
//
//}
