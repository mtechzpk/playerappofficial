package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.MainActivity;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.adapters.ChannelCategAdapter;
import com.mtechsoft.playerapp.adapters.MoviesTypeAdapter;
import com.mtechsoft.playerapp.models.ChannelCategModel;
import com.mtechsoft.playerapp.models.MoviesTypeModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment implements MoviesTypeAdapter.Callback{
    RecyclerView rvMoviesType;
    private MoviesTypeAdapter pAdapter;
    private ArrayList<MoviesTypeModel> moviesTypeModels;
    GridLayoutManager gridLayoutManager;
    int spacing = 50;
    Context context;
    int mov_cat_id;
    ProgressDialog progressdialog;
    boolean includeEdge = true;
    public MoviesFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_movies, container, false);
        initViews(v);
        progressdialog = new ProgressDialog(getActivity());

        getChannelCateg();

        return  v;
    }
    public void initViews(View v) {
        rvMoviesType = v.findViewById(R.id.rvMoviesType);


    }


    @Override
    public void onItemClick(int pos) {
        mov_cat_id = moviesTypeModels.get(pos).getMovies_cat_id();
        Utilities.saveInt(getContext(),"mov_cat_id",mov_cat_id);
        ((MainActivity) getActivity()).navController.navigate(R.id.action_moviesFragment_to_moviesFilterFragment);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getChannelCateg() {

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.GET, Server.GET_MOVIES_TYPE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {
                        moviesTypeModels = new ArrayList<>();
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            MoviesTypeModel moviesTypeModel = new MoviesTypeModel();

                            int id = jsonObject.getInt("id");
                            String movies_category = jsonObject.getString("movie_category_name");

                            moviesTypeModel.setMoviesCategoriesName(movies_category);
                            moviesTypeModel.setMovies_cat_id(id);


                            moviesTypeModels.add(moviesTypeModel);


                        }
                        pAdapter = new MoviesTypeAdapter(context, moviesTypeModels,MoviesFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                        rvMoviesType.setLayoutManager(linearLayoutManager);
                        rvMoviesType.setAdapter(pAdapter);


                    } else {

                        Toast.makeText(getActivity(), "No Movies Categories Available", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(mov_cat_id));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept","application/json");
                return params;            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }


}
