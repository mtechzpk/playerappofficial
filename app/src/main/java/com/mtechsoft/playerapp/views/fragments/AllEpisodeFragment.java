package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.DailyMotionActivity;
import com.mtechsoft.playerapp.activities.YoutubeActivity;
import com.mtechsoft.playerapp.adapters.DramasAdapter;
import com.mtechsoft.playerapp.adapters.EpisodesAdapter;
import com.mtechsoft.playerapp.models.DramasModel;
import com.mtechsoft.playerapp.models.EpisodesModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllEpisodeFragment extends Fragment implements EpisodesAdapter.Callback {
    RecyclerView rvEpisodes;
    private EpisodesAdapter pAdapter;
    private Context context;
    ImageView ivDramaProfile;
    JSONObject jsonObject;
    JSONArray objUser;
    private ArrayList<EpisodesModel> episodesModels;

    int ch_id;
    int chanel_Categ_id;
    int drama_id;
    String drama_thumnail1;

    public AllEpisodeFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_all_episode, container, false);
        initViews(v);
        getEpisodes();
        return v;
    }

    public void initViews(View v) {
        rvEpisodes = v.findViewById(R.id.rvEpisodes);
        ivDramaProfile = v.findViewById(R.id.ivDramaProfile);
        drama_thumnail1 = Utilities.getString((getContext()), "drama_photo");
        Picasso.get().load(drama_thumnail1).into(ivDramaProfile);

    }

    @Override
    public void onItemClick(int pos) throws JSONException {
        jsonObject = objUser.getJSONObject(pos);
        String url = jsonObject.getString("filename");
        String url_type = jsonObject.getString("url_type");

        if(url_type.equals("0")){
            Intent intent = new Intent(getActivity(), YoutubeActivity.class);
            intent.putExtra("data", url);
            startActivity(intent);
        }else if(url_type.equals("1")){
            Intent intent = new Intent(getActivity(), DailyMotionActivity.class);
            intent.putExtra("data", url);
            startActivity(intent);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getEpisodes() {
        ch_id = Utilities.getInt(getContext(), "ch_id");
        chanel_Categ_id = Utilities.getInt(getContext(), "Ch_cat_id");
        drama_id = Utilities.getInt(getContext(), "drama_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();

        final StringRequest episodesrequest = new StringRequest(Request.Method.POST, Server.EPISODES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {
                        episodesModels = new ArrayList<>();
                        // JSONObject obj = object.getJSONObject("result");
                        objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            jsonObject = objUser.getJSONObject(i);


                            int id = jsonObject.getInt("id");
                            String episodeNo = jsonObject.getString("number");
                            String thumbnail = jsonObject.getString("thumbnail");
                            String url_type = jsonObject.getString("url_type");


                            episodesModels.add(new EpisodesModel(thumbnail,id,episodeNo,url_type));


                        }
                        pAdapter = new EpisodesAdapter(context, episodesModels, AllEpisodeFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvEpisodes.setLayoutManager(linearLayoutManager);
                        rvEpisodes.setAdapter(pAdapter);


                    } else {

                        Toast.makeText(getActivity(), "No Episode Avilable", Toast.LENGTH_SHORT).show();
                    }
                    progressdialog.dismiss();
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("channel_category_id", String.valueOf(chanel_Categ_id));
                params.put("channel_id", String.valueOf(ch_id));
                params.put("drama_id", String.valueOf(drama_id));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        episodesrequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(episodesrequest);
    }

}
