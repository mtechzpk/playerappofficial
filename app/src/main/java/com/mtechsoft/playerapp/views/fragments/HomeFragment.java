package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtechsoft.playerapp.HomeImageModel;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.MainActivity;
import com.mtechsoft.playerapp.adapters.LatestMoviesAdapter;
import com.mtechsoft.playerapp.adapters.RecentlyAdedAdapter;
import com.mtechsoft.playerapp.adapters.SlidingImage_Adapter;
import com.mtechsoft.playerapp.models.LatestMoviesModel;
import com.mtechsoft.playerapp.models.RecentlyAddedModel;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements  SwipeRefreshLayout.OnRefreshListener {
    LinearLayout llDramas, llMovies;
    JSONObject jsonObject;
    JSONArray objUser;
    TextView tvEpisodetext,tvEmptyMovies,tvNoInternet,tvNoInternet1;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    Context context;
    int ch_id;
    int chanel_Categ_id;
    int drama_id;

    private RecyclerView rvRecentlyAdded, rvLatestMovies;
    private RecentlyAdedAdapter pAdapter;
    private LatestMoviesAdapter lmAdapter;
    private ArrayList<RecentlyAddedModel> recentlyAddedModels;
    private ArrayList<LatestMoviesModel> latestMoviesModels;

    Timer timer;


    public HomeFragment() {
        // Required empty public constructor
    }

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    private ArrayList<HomeImageModel> homeImageModels;

    private int[] myImageList = new int[]{R.drawable.slider_1, R.drawable.slider_2,
            R.drawable.slider_3, R.drawable.slider_2, R.drawable.slider_1, R.drawable.slider_3};

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
//
        initView(v);

        if(!isConnected(getActivity())){ buildDialog(getActivity()).show();
            tvNoInternet.setVisibility(View.VISIBLE);
            tvNoInternet1.setVisibility(View.VISIBLE);
        }
        else {
//            Toast.makeText(context, "Dashboard", Toast.LENGTH_SHORT).show();
        }
        mSwipeRefreshLayout.setOnRefreshListener(this);

        getRecentEpisodes();
        getLatestmovies();

        homeImageModels = new ArrayList<>();
        homeImageModels = populateList();

        mPager.setAdapter(new SlidingImage_Adapter(getContext(), homeImageModels));
        CirclePageIndicator indicator = (CirclePageIndicator)
                v.findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = homeImageModels.size();


// Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                if (position == myImageList.length - 1) {

                } else {

                }

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 3000, 3000);


        return v;
    }

    public void initView(View v) {
        mPager = v.findViewById(R.id.viewpager);
        rvRecentlyAdded = v.findViewById(R.id.rvRecentlyAdded);
        rvLatestMovies = v.findViewById(R.id.rvLatestMovies);
        llDramas = v.findViewById(R.id.llDramas);
        llMovies = v.findViewById(R.id.llMovies);
        llMovies = v.findViewById(R.id.llMovies);
        tvEpisodetext = v.findViewById(R.id.tvEmptyListBottom);
        tvEmptyMovies = v.findViewById(R.id.tvEmptyMovies);
        tvNoInternet = v.findViewById(R.id.tvNoInternet);
        tvNoInternet1 = v.findViewById(R.id.tvNoInternet1);
        mSwipeRefreshLayout = v.findViewById(R.id.mSwipeRefreshLayout);


        llDramas.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_channelCategFragment));
        llMovies.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_moviesFragment));
    }
    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                onRefreshfrag();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        },1000);

//        Toast.makeText(context, "jdfjdf", Toast.LENGTH_SHORT).show();
    }

public void onRefreshfrag(){
    getFragmentManager().beginTransaction().detach(this).attach(this).commit();

}
    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {

            if (getActivity() == null)
                return;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (currentPage == NUM_PAGES - 1) {
                        currentPage = 0;
                    } else {
                        currentPage++;
                    }
                    mPager.setCurrentItem(currentPage, true);

                }
            });
        }
    }

    private ArrayList<HomeImageModel> populateList() {

        ArrayList<HomeImageModel> list = new ArrayList<>();

        for (int i = 0; i < myImageList.length; i++) {
            HomeImageModel homeImageModel = new HomeImageModel();
            homeImageModel.setImage_drawable(myImageList[i]);
            list.add(homeImageModel);

        }

        return list;
    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
        if (timer != null) {
            timer.cancel();

        }
    }
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
        else return false;
        } else
        return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

             dialog.cancel();
            }
        });

        return builder;
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    private void getRecentEpisodes() {

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));
//        progressdialog.show();

        final StringRequest episodesrequest = new StringRequest(Request.Method.GET, Server.GET_RECENT_EPISODES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    recentlyAddedModels = new ArrayList<RecentlyAddedModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");

                    if (status == 200) {
                        progressdialog.dismiss();
                        objUser = object.getJSONArray("date");

                        for (int i = 0; i < objUser.length(); i++) {
                            jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String episodeNo = jsonObject.getString("number");
                            String thumbnail = jsonObject.getString("thumbnail");
                            String url = jsonObject.getString("filename");
                            String urlType = jsonObject.getString("url_type");

                            recentlyAddedModels.add(new RecentlyAddedModel(thumbnail, id, url, episodeNo,urlType));
                        }


                        pAdapter = new RecentlyAdedAdapter(getContext(), recentlyAddedModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
                        rvRecentlyAdded.setLayoutManager(linearLayoutManager);
                        rvRecentlyAdded.setAdapter(pAdapter);

                    } else {
                        Utilities.hideProgressDialog();
                        tvEpisodetext.setVisibility(View.VISIBLE);
                        rvRecentlyAdded.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "No Episode Available", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
               progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                }
                else if (volleyError instanceof NoConnectionError) {
                   message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("channel_category_id", String.valueOf(chanel_Categ_id));
//                params.put("channel_id", String.valueOf(ch_id));
//                params.put("drama_id", String.valueOf(drama_id));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        episodesrequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(episodesrequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    private void getLatestmovies() {

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));
//        progressdialog.show();


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.GET, Server.GET_RECENT_MOVIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    latestMoviesModels = new ArrayList<LatestMoviesModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {

                        progressdialog.dismiss();
                        objUser = object.getJSONArray("date");

                         for (int i = 0; i < objUser.length(); i++) {
                            jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("name");
                            String url = jsonObject.getString("url");
                            String thumbnail = jsonObject.getString("thumbnail");
                            String url_type = jsonObject.getString("url_type");

                            latestMoviesModels.add(new LatestMoviesModel(id, name, url, thumbnail,url_type));
                        }

//
                        lmAdapter = new LatestMoviesAdapter(getContext(), latestMoviesModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
                        rvLatestMovies.setLayoutManager(linearLayoutManager);
                        rvLatestMovies.setAdapter(lmAdapter);


                    } else {

                        tvEmptyMovies.setVisibility(View.VISIBLE);
                        rvLatestMovies.setVisibility(View.GONE);
                        Utilities.hideProgressDialog();
                        Toast.makeText(getActivity(), "No Movies Avilable", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("movie_category_id", String.valueOf(mov_cat_dd));
//                params.put("year_id", String.valueOf(yearsid));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }
}
