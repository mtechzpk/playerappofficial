package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.MainActivity;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.adapters.DramasAdapter;
import com.mtechsoft.playerapp.models.DramasModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class DramasFragment extends Fragment implements DramasAdapter.Callback {
    RecyclerView rvDramas;
    private DramasAdapter pAdapter;
    private ArrayList<DramasModel> dramasModels;
    Context context;

    public DramasFragment() {
        // Required empty public constructor
    }

    String drama_thumnail;
    int ch_id;
    int chanel_Categ_id;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dramas, container, false);
        initViews(view);
        getDramas();

        return view;
    }

    public void initViews(View v) {
        rvDramas = v.findViewById(R.id.rvDramas);


    }
//    @Override
//    public void onItemClick(int pos) {
////        QuizFragment.chapterId = String.valueOf(chapters.get(pos).getId());
////        ChapterDetailFragment.chapter_name = String.valueOf(chapters.get(pos).getChapterName());
//
//        ((MainActivity) getActivity()).navController.navigate(R.id.action_dramasFragment_to_allEpisodeFragment);
//    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getDramas() {

        ch_id = Utilities.getInt(getContext(), "ch_id");
        chanel_Categ_id = Utilities.getInt(getContext(), "Ch_cat_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();


        final StringRequest dramarequest = new StringRequest(Request.Method.POST, Server.DRAMA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    dramasModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {

                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

//                            DramasModel dramasModel = new DramasModel();

                            int id = jsonObject.getInt("id");
                            String drama_name = jsonObject.getString("drama_name");
                            String drama_photo = jsonObject.getString("filename");
                            String total_episodes = jsonObject.getString("total_episodes");

//                            Utilities.saveString(getContext(), "drama_photo1", drama_photo);
                            dramasModels.add(new DramasModel(drama_name,drama_photo,id,total_episodes));

                        }
                        pAdapter = new DramasAdapter(context, dramasModels, DramasFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvDramas.setLayoutManager(linearLayoutManager);
                        rvDramas.setAdapter(pAdapter);


                    } else {

                        Toast.makeText(getActivity(), "No Drama Avilable", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("channel_category_id", String.valueOf(chanel_Categ_id));
                params.put("channel_id", String.valueOf(ch_id));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        dramarequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(dramarequest);
    }

    @Override
    public void onItemClick(int pos) {
        int drmamaId = dramasModels.get(pos).getDramaId();
        Utilities.saveInt(getContext(), "drama_id", drmamaId);
        String drmamaPhoto = dramasModels.get(pos).getPhoto();
        Utilities.saveString(getContext(), "drama_photo", drmamaPhoto);
        ((MainActivity) getActivity()).navController.navigate(R.id.action_dramasFragment_to_allEpisodeFragment);

    }
}
