package com.mtechsoft.playerapp.views.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mtechsoft.playerapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LandingFragment extends Fragment {
Button bSignup,bLogin;

    public LandingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_landing, container, false);

        initviews(view);
        return  view;
    }
public void initviews(View v){
        bLogin=v.findViewById(R.id.bLogin);
        bSignup=v.findViewById(R.id.bSignup);
        initNav();

}

public void initNav(){
        bLogin.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_landingFragment_to_loginFragment));
        bSignup.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_landingFragment_to_signUpFragment));

}

}
