package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.SessionManager.SessionManager;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.LandingActivity;
import com.mtechsoft.playerapp.activities.MainActivity;
import com.mtechsoft.playerapp.adapters.ChannelCategAdapter;
import com.mtechsoft.playerapp.api.ApiModelClass;
import com.mtechsoft.playerapp.api.ServerCallback;
import com.mtechsoft.playerapp.models.ChannelCategModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    EditText etEmail, etPassword;
    Button bLogin;
    LinearLayout llSignup;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initviews(view);


        return view;
    }

    public void initviews(View v) {
        bLogin = v.findViewById(R.id.bLogin);
        llSignup = v.findViewById(R.id.llSignup);
        etEmail = v.findViewById(R.id.etEmail);
        etPassword = v.findViewById(R.id.etPassword);
        initNav();

    }

    public void initNav() {
        llSignup.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_loginFragment_to_signUpFragment));
        bLogin.setOnClickListener(this);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bLogin:
                String userEmail = etEmail.getText().toString();
                String userPassword = etPassword.getText().toString();


                if (!userEmail.isEmpty()) {
                    if (!userPassword.isEmpty()) {
                        LoginApi(userEmail, userPassword);


                    } else {

                        Toast.makeText(getActivity(), "Password required", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(getActivity(), "Email required", Toast.LENGTH_SHORT).show();
                }


                break;
            case  R.id.llSignup:
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void LoginApi(final String email, final String pass) {

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();

        Map<String, String> postParam = new HashMap<String, String>();

        postParam.put("email", email);
        postParam.put("password", pass);


        HashMap<String, String> headers = new HashMap<String, String>();

        headers.put("Accept", "application/json");

        ApiModelClass.GetApiResponse(Request.Method.POST, Server.LOGIN, getContext(), postParam, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {


                if (ERROR.isEmpty()) {

                    try {

                        JSONObject object = new JSONObject(String.valueOf(result));
                        int status = object.getInt("status");
                        if (status==200) {
                            JSONObject data = object.getJSONObject("data");
                            SessionManager sessionManager = new SessionManager(getActivity());
                            int id = data.getInt("id");
                            String email = data.getString("email");
                            String name = data.getString("name");


                            Utilities.saveString(getContext(), "user_email", email);
                            Utilities.saveString(getContext(), "user_name", name);
                            Utilities.saveInt(getContext(), "user_id", id);


                            String msg = object.getString("message");

                            Utilities.hideProgressDialog();
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            sessionManager.createLoginSession(name,email,id);
                            startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();

                        } else {
                            Utilities.hideProgressDialog();
                            if (status==400) {
                                String error = object.getString("message");
                                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

                    Utilities.hideProgressDialog();
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();
                }
            }
        });


    }



}
