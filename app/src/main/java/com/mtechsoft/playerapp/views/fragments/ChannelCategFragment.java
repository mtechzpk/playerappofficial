package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.MainActivity;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.adapters.ChannelCategAdapter;
import com.mtechsoft.playerapp.models.ChannelCategModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChannelCategFragment extends Fragment implements ChannelCategAdapter.Callback{
    RecyclerView rvChannelsCateg;
    private ChannelCategAdapter pAdapter;
    private ArrayList<ChannelCategModel> channelCategModels;
    Context context;
    int ch_cat_id;

    boolean includeEdge = true;
    public ChannelCategFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_channel_categ, container, false);
        initViews(v);
        getChannelCateg();

        return  v;
    }
    public void initViews(View v) {
        rvChannelsCateg = v.findViewById(R.id.rvChannelsCateg);


    }
    @Override
    public void onItemClick(int pos) {


        ch_cat_id = channelCategModels.get(pos).getChannel_category_id();
        Utilities.saveInt(getContext(),"Ch_cat_id",ch_cat_id);

        ((MainActivity) getActivity()).navController.navigate(R.id.action_channelCategFragment_to_channelFragment);
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getChannelCateg() {


        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.GET, Server.CHANNEL_CATEGORIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {
                        channelCategModels = new ArrayList<>();
                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            ChannelCategModel channelCategModel = new ChannelCategModel();

                            int id = jsonObject.getInt("id");
                            String ch_category = jsonObject.getString("channel_category_name");

                            channelCategModel.setChannelCategName(ch_category);
                            channelCategModel.setChannel_category_id(id);


                            channelCategModels.add(channelCategModel);


                        }
                        pAdapter = new ChannelCategAdapter(context, channelCategModels,ChannelCategFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                        rvChannelsCateg.setLayoutManager(linearLayoutManager);
                        rvChannelsCateg.setAdapter(pAdapter);


                    } else {

                        Toast.makeText(getActivity(), "No Channel Available", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(ch_cat_id));
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }
}
