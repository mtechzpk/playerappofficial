package com.mtechsoft.playerapp.views.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.MySingleton;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.SessionManager.SessionManager;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.activities.DailyMotionActivity;
import com.mtechsoft.playerapp.activities.MainActivity;
import com.mtechsoft.playerapp.activities.YoutubeActivity;
import com.mtechsoft.playerapp.adapters.AllChannelAdapter;
import com.mtechsoft.playerapp.adapters.DramasAdapter;
import com.mtechsoft.playerapp.adapters.EpisodesAdapter;
import com.mtechsoft.playerapp.adapters.LatestMoviesAdapter;
import com.mtechsoft.playerapp.adapters.MoviesAdapter;
import com.mtechsoft.playerapp.adapters.MoviesFilterFAdapter;
import com.mtechsoft.playerapp.adapters.RecentlyAdedAdapter;
import com.mtechsoft.playerapp.models.ChannelModel;
import com.mtechsoft.playerapp.models.DramasModel;
import com.mtechsoft.playerapp.models.EpisodesModel;
import com.mtechsoft.playerapp.models.LatestMoviesModel;
import com.mtechsoft.playerapp.models.MoviesFilterModel;
import com.mtechsoft.playerapp.models.RecentlyAddedModel;
import com.mtechsoft.playerapp.models.YearListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFilterFragment extends Fragment implements MoviesFilterFAdapter.Callback {
    RecyclerView rvMoviesFilter;
    Button bApplyFilter;
    TextView tvMoviestext;
    private MoviesFilterFAdapter pAdapter;
    private ArrayList<MoviesFilterModel> moviesFilterModels;
    private ArrayList<YearListModel> yearListModels;
    private List<String> year_name_list;
    JSONObject jsonObject;
    JSONArray objUser;
    Spinner spYearMovies;
    int mov_cat_dd;
    int yearsid;

    private RecyclerView rvLatestMovies;
    private MoviesAdapter lmAdapter;
    private ArrayList<LatestMoviesModel> latestMoviesModels;


    ArrayList<String> YearName;
    GridLayoutManager gridLayoutManager;
    String year;

//    chanel_cat_dd=Utilities.getInt(getContext(),"Ch_cat_id");

    // Initializing a String Array

    public MoviesFilterFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_movies_filter, container, false);
        initViews(v);
        getyear_list();
//        getmovies(5);
        getCategoriesmovies();
        Calendar calendar = Calendar.getInstance();
//        int year = calendar.get(Calendar.YEAR);

        bApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (year.equals("Select a year")) {
                    Toast.makeText(getActivity(), "Please Select a year first.", Toast.LENGTH_SHORT).show();
                } else {
                    getyear_id(year);
                }

            }
        });

        spYearMovies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                year = spYearMovies.getItemAtPosition(spYearMovies.getSelectedItemPosition()).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        return v;
    }


    private void getyear_id(final String year) {

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.GET_YEARID, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {
                        progressdialog.dismiss();
                        JSONObject data = object.getJSONObject("data");
                        int id = data.getInt("id");
//                         yearsid = data.getInt("id");
                        getmovies(id);
                    } else {

//                        Toast.makeText(getActivity(), "No Channel Avilable", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("year", year);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };


        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }

    public void initViews(View v) {
        rvLatestMovies = v.findViewById(R.id.rvMoviesFilter);
        spYearMovies = v.findViewById(R.id.spYearMovies);
        bApplyFilter = v.findViewById(R.id.bApplyFilter);
        tvMoviestext = v.findViewById(R.id.tvNoMoviestext);


//        Paper.init(getActivity());

    }

    @Override
    public void onItemClick(int pos) throws JSONException {
        jsonObject = objUser.getJSONObject(pos);
        String url = jsonObject.getString("url");
        String url_type = jsonObject.getString("url_type");

        if(url_type.equals("0")){
            Intent intent = new Intent(getActivity(), DailyMotionActivity.class);
            intent.putExtra("data", url);
            startActivity(intent);
        }else if(url_type.equals("1")){
            Intent intent = new Intent(getActivity(), DailyMotionActivity.class);
            intent.putExtra("data", url);
            startActivity(intent);
        }
        else{


        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getyear_list() {

        mov_cat_dd = Utilities.getInt(getContext(), "mov_cat_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));
        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.GET_YEAR_MOVIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {
                        yearListModels = new ArrayList<YearListModel>();
                        year_name_list = new ArrayList<String>();
                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String year_name = jsonObject.getString("year");
                            Utilities.saveString(getContext(), "yearName", year_name);
                            year_name_list.add(year_name);
                            yearListModels.add(new YearListModel(year_name, id));
                        }
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                                getActivity(), R.layout.spinner_item, year_name_list
                        );

                        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
                        spYearMovies.setAdapter(spinnerArrayAdapter);
//                        Collections.reverse(year_name_list);
                        year_name_list.add(0, "Select a year");
                        spYearMovies.setSelection(0);

                    } else {

                        Toast.makeText(getActivity(), "No Movies Avilable", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("movie_category_id", String.valueOf(mov_cat_dd));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };


        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    private void getmovies(final int yearsid) {


        mov_cat_dd = Utilities.getInt(getContext(), "mov_cat_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));

        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.GET_MOVIE_BY_YEARS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    moviesFilterModels = new ArrayList<MoviesFilterModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {
                        objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
//
                            jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("name");
                            String url = jsonObject.getString("url");
                            String thumbnail = jsonObject.getString("thumbnail");

                            moviesFilterModels.add(new MoviesFilterModel(id, name, url, thumbnail));
                            progressdialog.dismiss();
                        }
                        pAdapter = new MoviesFilterFAdapter(getContext(), moviesFilterModels, MoviesFilterFragment.this);
                        GridLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 2);
                        rvLatestMovies.setLayoutManager(linearLayoutManager);
                        rvLatestMovies.setAdapter(pAdapter);
                        rvLatestMovies.setVisibility(View.VISIBLE);
                        tvMoviestext.setVisibility(View.GONE);

                    } else {

                        progressdialog.dismiss();
                        tvMoviestext.setVisibility(View.VISIBLE);
                        rvLatestMovies.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "No Movies Avilable", Toast.LENGTH_SHORT).show();

                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("movie_category_id", String.valueOf(mov_cat_dd));
                params.put("year_id", String.valueOf(yearsid));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };


        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    private void getCategoriesmovies() {


        mov_cat_dd = Utilities.getInt(getContext(), "mov_cat_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim1, null));
//        progressdialog.show();


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.GET_MOVIES_BY_CATEG, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    latestMoviesModels = new ArrayList<LatestMoviesModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String msg = object.getString("message");
                    if (status == 200) {

                        progressdialog.dismiss();
                        objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
                            jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("name");
                            String url = jsonObject.getString("url");
                            String year_id = jsonObject.getString("year_id");
                            String url_type = jsonObject.getString("url_type");

                            Utilities.saveString(getContext(), "year_id", year_id);

                            String thumbnail = jsonObject.getString("thumbnail");
                            latestMoviesModels.add(new LatestMoviesModel(id,name,url,thumbnail,url_type));
                        }

//
                        lmAdapter = new MoviesAdapter(getContext(), latestMoviesModels);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
                        rvLatestMovies.setLayoutManager(layoutManager);
                        rvLatestMovies.setAdapter(lmAdapter);


                    } else {
                        progressdialog.dismiss();
                        Toast.makeText(getActivity(), "No Movies Avilable", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("movie_category_id", String.valueOf(mov_cat_dd));
//                params.put("year_id", String.valueOf(yearsid));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }

}
