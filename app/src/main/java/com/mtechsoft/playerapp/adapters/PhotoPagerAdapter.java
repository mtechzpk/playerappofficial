package com.mtechsoft.playerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.mtechsoft.playerapp.R;


public class PhotoPagerAdapter extends PagerAdapter {
    private int[] image_resource = {R.drawable.slider_1, R.drawable.slider_2, R.drawable.slider_1};
    private Context ctx;
    private LayoutInflater layoutinflater;

    public PhotoPagerAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return image_resource.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return (view == (LinearLayout) o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutinflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view_item = layoutinflater.inflate(R.layout.swipe_layout, container, false);
        ImageView imageView = view_item.findViewById(R.id.vpSlider);
     //   TextView textView = view_item.findViewById(R.id.tv_image_count);
        imageView.setImageResource(image_resource[position]);
        //textView.setText("image:" + position);
        container.addView(view_item);
        return view_item;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}
