package com.mtechsoft.playerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.models.ChannelModel;
import com.mtechsoft.playerapp.models.DramasModel;
import com.mtechsoft.playerapp.models.EpisodesModel;
import com.mtechsoft.playerapp.models.RecentlyAddedModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;

public class EpisodesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private  Callback callback;

    private ArrayList<EpisodesModel> episodesModels;

    public EpisodesAdapter(Context context, ArrayList<EpisodesModel> episodesModels,Callback callback) {
        this.context = context;
        this.callback=callback;
        this.episodesModels = episodesModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_episodes_list, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.tvEpisodeN0.setText(episodesModels.get(position).getEpisodeNo());

        Picasso.get().load(episodesModels.get(position).getPhoto()).into(holder1.ivThumbnail);
        holder1.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return episodesModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        ImageView ivThumbnail;
        TextView tvEpisodeN0;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.ivEpisodeThumbnail);
            tvEpisodeN0 = itemView.findViewById(R.id.tvEpisodeNo);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            EpisodesModel poem = episodesModels.get(pos);
            tvEpisodeN0.setText(poem.getEpisodeNo());
            initClickListener();
        }

        private void initClickListener() {
//
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        callback.onItemClick(getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }
}
