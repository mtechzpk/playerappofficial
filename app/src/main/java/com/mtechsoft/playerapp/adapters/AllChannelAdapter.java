package com.mtechsoft.playerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.models.ChannelModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


import de.hdodenhof.circleimageview.CircleImageView;


public class AllChannelAdapter extends RecyclerView.Adapter<AllChannelAdapter.MyViewHolder> {

    ArrayList<ChannelModel> channelModels;
    Context context;
    Callback callback;


    public AllChannelAdapter(ArrayList<ChannelModel> channelModels, Context context, Callback callback) {
        this.channelModels = channelModels;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.row_channels, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.tvChannelName.setText(channelModels.get(position).getChannelName());
        Picasso.get().load(channelModels.get(position).getPhoto()).into(holder.ivThumbnail);


        holder.initClickListener();


    }

    @Override
    public int getItemCount() {

        return channelModels.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView ivThumbnail;
        TextView tvChannelName;
        LinearLayout llItem;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.ivChannelProfile);
            llItem = itemView.findViewById(R.id.llItem);
            tvChannelName = itemView.findViewById(R.id.tvChannelName);


        }

        private void initClickListener() {

            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }


    }

    public interface Callback {
        void onItemClick(int pos);
    }

}
