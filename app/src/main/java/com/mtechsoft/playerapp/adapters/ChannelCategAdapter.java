package com.mtechsoft.playerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.models.ChannelCategModel;
import com.mtechsoft.playerapp.models.DramasModel;
import com.mtechsoft.playerapp.models.MoviesTypeModel;

import java.util.ArrayList;

public class ChannelCategAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ChannelCategModel> channelCategModels;
    private Callback callback;

    public ChannelCategAdapter(Context context, ArrayList<ChannelCategModel> channelCategModels, Callback callback) {
        this.context = context;
        this.callback = callback;
        this.channelCategModels = channelCategModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_channel_categ, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        ChannelCategModel channelCategModel = channelCategModels.get(position);
        holder1.tvChannelCateg.setText(channelCategModel.getChannelCategName());

        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return channelCategModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvChannelCateg;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvChannelCateg = itemView.findViewById(R.id.tvChannelCateg);
            llItem = itemView.findViewById(R.id.llItem);


        }

        private void bind(int pos) {
            ChannelCategModel channelCategModel = channelCategModels.get(pos);
//            tvChannelCateg.setText(channelCategModel.getChannelCategName());
            initClickListener();
        }

        private void initClickListener() {

            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }
}
