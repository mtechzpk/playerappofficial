package com.mtechsoft.playerapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.activities.DailyMotionActivity;
import com.mtechsoft.playerapp.activities.YoutubeActivity;
import com.mtechsoft.playerapp.models.ChannelModel;
import com.mtechsoft.playerapp.models.DramasModel;
import com.mtechsoft.playerapp.models.RecentlyAddedModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;

public class RecentlyAdedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private  Callback callback;

    private ArrayList<RecentlyAddedModel> recentlyAddedModels;

    public RecentlyAdedAdapter(Context context, ArrayList<RecentlyAddedModel> recentlyAddedModels) {
        this.context = context;
        this.recentlyAddedModels = recentlyAddedModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recently_added, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.tvEpisodeN0.setText(recentlyAddedModels.get(position).getEpisodeNo());
        Picasso.get().load(recentlyAddedModels.get(position).getPhoto()).into(holder1.ivThumbnail);
        holder1.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = recentlyAddedModels.get(position).getUrl();
                String urlType = recentlyAddedModels.get(position).getUrl_type();

                if (urlType.equals("0")){

                    Intent intent = new Intent(context, YoutubeActivity.class);
                    intent.putExtra("data",url);
                    context.startActivity(intent);
                }else if(urlType.equals("1")){
                    Intent intent = new Intent(context, DailyMotionActivity.class);
                    intent.putExtra("data",url);
                    context.startActivity(intent);
//                    ((Activity)context).finish();


                }

            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return recentlyAddedModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        ImageView ivThumbnail;
        TextView tvEpisodeN0;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            tvEpisodeN0 = itemView.findViewById(R.id.tvEpisodeNo);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            RecentlyAddedModel poem = recentlyAddedModels.get(pos);
            tvEpisodeN0.setText(poem.getEpisodeNo());
//            initClickListener();
        }

        private void initClickListener() {
//
//            llItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    try {
//                        callback.onItemClick(getAdapterPosition());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }
}
