package com.mtechsoft.playerapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.activities.DailyMotionActivity;
import com.mtechsoft.playerapp.activities.YoutubeActivity;
import com.mtechsoft.playerapp.models.LatestMoviesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<LatestMoviesModel> latestMoviesModels;
    private Callback callback;

    public MoviesAdapter(Context context, ArrayList<LatestMoviesModel> latestMoviesModels) {
        this.context = context;
        this.callback = callback;
        this.latestMoviesModels = latestMoviesModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_movies_filter, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvMoviesName.setText(latestMoviesModels.get(position).getMovieName());
        Picasso.get().load(latestMoviesModels.get(position).getThumbnail()).into(holder1.ivThumbnail);

        holder1.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = latestMoviesModels.get(position).getUrl();
                String urlType = latestMoviesModels.get(position).getUrl_type();
                if (urlType.equals("0")){

                    Intent intent = new Intent(context, YoutubeActivity.class);
                    intent.putExtra("data",url);
                    context.startActivity(intent);
                }else if(urlType.equals("1")){
                    Intent intent = new Intent(context, DailyMotionActivity.class);
                    intent.putExtra("data",url);
                    context.startActivity(intent);

                }


            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return latestMoviesModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        ImageView ivThumbnail;
        LinearLayout llItem;
        TextView tvMoviesName;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.ivProfile);
            tvMoviesName = itemView.findViewById(R.id.tvMoviesName);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            LatestMoviesModel latestMoviesModel = latestMoviesModels.get(pos);
//            initClickListener();
        }

        private void initClickListener() {
//
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }
}
