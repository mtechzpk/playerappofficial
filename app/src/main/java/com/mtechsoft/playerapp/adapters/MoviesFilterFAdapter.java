package com.mtechsoft.playerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.models.EpisodesModel;
import com.mtechsoft.playerapp.models.MoviesFilterModel;

import org.json.JSONException;

import java.util.ArrayList;

public class MoviesFilterFAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<MoviesFilterModel> moviesFilterModels;
    private Callback callback;

    public MoviesFilterFAdapter(Context context, ArrayList<MoviesFilterModel> moviesFilterModels, Callback callback) {
        this.context = context;
        this.callback = callback;
        this.moviesFilterModels = moviesFilterModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_movies_filter, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvMoviesName.setText(moviesFilterModels.get(position).getMovieName());


        holder1.bind(position, (BookViewHolder) holder);
    }

    @Override
    public int getItemCount() {
        return moviesFilterModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        ImageView ivThumbnail;
        TextView tvMoviesName;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.ivProfile);
            llItem = itemView.findViewById(R.id.llItem);
            tvMoviesName = itemView.findViewById(R.id.tvMoviesName);

        }

        private void bind(int pos, BookViewHolder holder) {

            Glide.with(context).load(moviesFilterModels.get(pos).getThumbnail()).centerCrop().into(holder.ivThumbnail);

            initClickListener();
        }

        private void initClickListener() {
//
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        callback.onItemClick(getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }
}
