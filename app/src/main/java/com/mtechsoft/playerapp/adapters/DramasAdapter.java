package com.mtechsoft.playerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.playerapp.R;
import com.mtechsoft.playerapp.Server.Server;
import com.mtechsoft.playerapp.Utilities;
import com.mtechsoft.playerapp.models.ChannelModel;
import com.mtechsoft.playerapp.models.DramasModel;
import com.mtechsoft.playerapp.models.RecentlyAddedModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DramasAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
   private  Callback callback;
    private ArrayList<DramasModel> dramasModels;

    public DramasAdapter(Context context, ArrayList<DramasModel> dramasModels,Callback callback) {
        this.context = context;
        this.callback=callback;
        this.dramasModels = dramasModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dramas_list, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvDrmamaName.setText(dramasModels.get(position).getDramaName());
        holder1.tvEpisodeN0.setText("Ep(s): "+dramasModels.get(position).getTotalEpisodes());
        Picasso.get().load(dramasModels.get(position).getPhoto()).into(holder1.ivThumbnail);
        holder1.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//


                Navigation.createNavigateOnClickListener(R.id.action_dramasFragment_to_allEpisodeFragment);

            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return dramasModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        ImageView ivThumbnail;
        TextView tvDrmamaName;
        TextView tvEpisodeN0;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.ivDramaThumbnail);
            tvDrmamaName = itemView.findViewById(R.id.tvDramaName);
            tvEpisodeN0 = itemView.findViewById(R.id.tvTotalEpsodes);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            DramasModel poem = dramasModels.get(pos);
            tvDrmamaName.setText(poem.getDramaName());
            initClickListener();
        }

        private void initClickListener() {
//
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }
}
