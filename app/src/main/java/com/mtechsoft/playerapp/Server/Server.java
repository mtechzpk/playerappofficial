package com.mtechsoft.playerapp.Server;

public class Server {
//    public static final String BASE_URL = "http://ubr.educationtracker.website/";
    public static final String BASE_URL = "http://siasat-pk.com/public/api/";
//    public static final String BASE_URL = "http://172.104.217.178/chatting_app/api/";
    public static final String BASE_PHOTO = "http://travces.com/drama/public/api";

    public static final String CHANNEL_CATEGORIES = BASE_URL + "get_channel_categories";
    public static final String CHANNEL = BASE_URL + "get_channels";
    public static final String DRAMA = BASE_URL + "get_dramas";
    public static final String SIGNUP = BASE_URL + "register";
    public static final String LOGIN = BASE_URL + "login";
    public static final String EPISODES = BASE_URL + "get_episodes";
    public static final String GET_MOVIES_TYPE = BASE_URL + "get_movie_categories";
    public static final String GET_YEAR_MOVIES = BASE_URL + "get_years";
    public static final String GET_MOVIE_BY_YEARS = BASE_URL + "get_movies";
    public static final String GET_RECENT_EPISODES = BASE_URL + "recent_dramas";
    public static final String GET_RECENT_MOVIES = BASE_URL + "recent_movies";
    public static final String GET_YEARID= BASE_URL + "get_year_id";
    public static final String GET_MOVIES_BY_CATEG= BASE_URL + "get_category_movies";


}
