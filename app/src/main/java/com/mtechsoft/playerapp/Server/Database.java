package com.mtechsoft.playerapp.Server;

public class Database {
    public static final String NODE_USERS = "users";
    public static final String NODE_USER_CHATS = "users-chats";
    public static final String NODE_MESSAGES = "messages";
}
